from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('addKegiatan/', views.addKegiatan, name='addKegiatan'),
    path('addPeserta/<int:id>', views.addPeserta, name='addPeserta')
]
