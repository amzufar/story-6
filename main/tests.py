from django.test import TestCase, Client
from django.urls import resolve
from .views import home, addKegiatan, addPeserta
from .models import Kegiatan, Peserta

# Create your tests here.
class TestIndex(TestCase):
	def test_index_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_index_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'main/home.html')

class TestAddEvent(TestCase):
	def test_add_Event_url_is_exist(self):
		response = Client().get('/addKegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_add_Event_index_func(self):
		found = resolve('/addKegiatan/')
		self.assertEqual(found.func, addKegiatan)

	def test_add_Event_using_template(self):
		response = Client().get('/addKegiatan/')
		self.assertTemplateUsed(response, 'main/addKegiatan.html')

	def test_Event_model_create_new_object(self):
		acara = Kegiatan(nama_kegiatan ="abc")
		acara.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)

	def test_add_KegiatanPost(self):
		response = Client().post('/addKegiatan/', {"nama_kegiatan" : "abcd", "desc_kegiatan" : "ah mntap"})
		self.assertEqual(response.status_code, 302)

class TestAddMember(TestCase):
	def setUp(self):
		acara = Kegiatan(nama_kegiatan ="abc", desc_kegiatan='ah mntap')
		acara.save()

	def test_add_Member_url_is_exist(self):
		response = Client().post('/addPeserta/1', data={'nama_peserta':'amju'})
		self.assertEqual(response.status_code, 302)
	
	def test_add_Person_Post(self):
		response = Client().post('/addPeserta/1', {"nama_peserta" : "yeyeye"})
		self.assertEqual(response.status_code, 302)
	

class TestModel(TestCase):
	def test_str_model_event(self):
		kegiatan = Kegiatan.objects.create(nama_kegiatan='pepewe', desc_kegiatan='yeyeye')
		self.assertEqual(kegiatan.__str__(), 'pepewe')

	def test_str_model_member(self):
		kegiatan = Kegiatan.objects.create(nama_kegiatan='pepewe')
		peserta = Peserta.objects.create(nama_peserta='memberTest', kegiatan=kegiatan)
		self.assertEqual(peserta.__str__(), 'memberTest')



# from django.test import TestCase, Client
# from django.urls import resolve
# from .views import home, addKegiatan, addPeserta
# from .models import Kegiatan, Peserta

# class TestIndex(TestCase):
#     def test_index_url_is_exist(self):
#         response = Client().get('/')
#         self.assertEqual(response.status_code, 200)

#     def test_index_index_func(self):
#         found = resolve('/')
#         self.assertEqual(found.func, home)

#     def test_index_using_template(self):
#         response = Client().get('/')
#         self.assertTemplateUsed(response, 'main/home.html')

# class TestAddKegiatan(TestCase):
#     def test_add_Kegiatan_url_is_exist(self):
#         response = Client().get('/addKegiatan/')
#         self.assertEqual(response.status_code, 200)
    
#     def test_add_Kegiatan_index_func(self):
#         found = resolve('/addKegiatan/')
#         self.assertEqual(found.func, addKegiatan)
    
#     def test_add_Kegiatan_using_template(self):
#         response = Client().get('/addKegiatan/')
#         self.assertTemplateUsed(response, 'main/addKegiatan.html')
    
#     def test_add_KegiatanPost(self):
#         response = Client().post('/addKegiatan/', {"nama_kegiatan" : "abcd", "desc_kegiatan" : "ah mntap"})
#         self.assertEqual(response.status_code, 302)

# class TestAddPeserta(TestCase):
#     def setUp(self):
#         event = Kegiatan(nama_kegiatan = "abc", desc_kegiatan="ah mntap")
#         event.save()
    
#     def test_add_Peserta_url_is_exist(self):
#         response = Client().post('/addPeserta/1/', data={"nama_peserta" : "ammar"})
#         self.assertEqual(response.status_code, 302)
    
#     def test_add_Peserta_Post(self):
#         response = Client().post('/addPeserta/1/', {"nama_peserta" : "lance"})
#         self.assertEqual(response.status_code, 302)

# class TestModel(TestCase):
#     def test_str_model_event(self):
#         kegiatan = Kegiatan.objets.create(nama_kegiatan='ppw', desc_kegiatan='ppw is fun')
#         self.assertEqual(kegiatan.__str__(), 'ppw')

#     def test_str_model_peserta(self):
#         kegiatan = Kegiatan.objects.create(nama_kegiatan='ppw')
#         peserta = Peserta.objects.create(nama_peserta='memberTest', kegiatan = kegiatan)
#         self.assertEqual(peserta.__str__(), 'memberTest')




# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


# class MainTestCase(TestCase):
#     def test_root_url_status_200(self):
#         response = self.client.get('/')
#         self.assertEqual(response.status_code, 200)
#         # You can also use path names instead of explicit paths.
#         response = self.client.get(reverse('main:home'))
#         self.assertEqual(response.status_code, 200)


# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
