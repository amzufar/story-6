from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm


def home(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    context = {'peserta' : peserta, 'kegiatan' : kegiatan}
    return render(request, 'main/home.html', context)

def addKegiatan(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('main:home')
    
    else:
        form = KegiatanForm()
    
    context = {'form' : form}
    return render(request, 'main/addKegiatan.html', context)

def addPeserta(request, id):
    if request.method == 'POST':
        form = PesertaForm(request.POST)

        if form.is_valid():
            form = Peserta(kegiatan = Kegiatan.objects.get(id=id), nama_peserta = form.data['nama_peserta'])
            form.save()
            return redirect('main:home')
    
    else:
        form = PesertaForm()
    
    context = {'form' : form}
    return render(request, 'main/addPeserta.html', context)


